package controllers

import (
	"context"
	"net/http"
	"strconv"

	article "bitbucket.org/b0ralgin/micro_API/article"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"

	pg "github.com/go-pg/pg"
)

type articleController struct {
	client client.Client
}

func NewArticleController(db *pg.DB, client client.Client) *articleController {
	return &articleController{
		client: client,
	}
}

func (a *articleController) FindBy(perPage int) func(c echo.Context) error {
	return func(c echo.Context) error {
		page, err := strconv.ParseInt(c.QueryParam("page"), 10, 64)
		if err != nil {
			page = 0
		}
		search := c.QueryParam("search")
		db := article.NewArticleGetterService("go.micro.news", a.client)
		filter := &article.Filter{
			Page:    int32(page),
			PerPage: int32(perPage),
			Search:  search,
		}
		articles, err := db.GetArticles(context.TODO(), filter)
		if err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}
		return c.JSON(http.StatusOK, articles)
	}
}
