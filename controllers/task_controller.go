package controllers

import (
	"net/http"

	"bitbucket.org/b0ralgin/micro_news_gateway/repository"
	"bitbucket.org/b0ralgin/micro_news_gateway/services"
	"bitbucket.org/b0ralgin/micro_news_gateway/types"
	pg "github.com/go-pg/pg"
	"github.com/labstack/echo"
	micro "github.com/micro/go-micro"
)

type TaskController struct {
	db   *pg.DB
	pubs map[string]micro.Publisher
}

func NewTaskController(db *pg.DB, pubs map[string]micro.Publisher) *TaskController {
	return &TaskController{
		db:   db,
		pubs: pubs,
	}
}

func (t *TaskController) AddRssAggregator(c echo.Context) error {
	aggregator := types.Aggregator{}
	if err := c.Bind(&aggregator); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	aggregator.Type = "rss"
	if !aggregator.Valid() {
		return c.JSON(http.StatusBadRequest, struct{ Message string }{Message: "Request is invalid"})
	}

	return t.addAggregator(aggregator, t.pubs["rss"], c)
}

func (t *TaskController) AddHtmlAggregator(c echo.Context) error {
	aggregator := types.Aggregator{}
	if err := c.Bind(&aggregator); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	aggregator.Type = "html"
	if !aggregator.HtmlValid() {
		return c.JSON(http.StatusBadRequest, struct{ Message string }{Message: "Request is invalid"})
	}
	return t.addAggregator(aggregator, t.pubs["html"], c)
}

func (t *TaskController) addAggregator(aggregator types.Aggregator, pub micro.Publisher, c echo.Context) error {
	id, err := repository.AddRssAggregator(&aggregator, t.db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	go services.RunAggregator(aggregator, pub)
	return c.JSON(http.StatusOK, struct{ Id int64 }{Id: id})
}
