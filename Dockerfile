FROM b0ralgin/glide

ENV APP_PATH=/go/src/bitbucket.org/b0ralgin/micro_news_gateway/

RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

COPY micro_news_gateway/glide.yaml glide.yaml
COPY micro_news_gateway/glide.lock glide.lock
RUN glide install -v

ADD micro_news_gateway/ $APP_PATH
RUN CGO_ENABLED=0 go build -o gateway
CMD ./gateway

EXPOSE 3000/tcp