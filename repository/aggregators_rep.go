package repository

import (
	"bitbucket.org/b0ralgin/micro_news_gateway/types"
	pg "github.com/go-pg/pg"
)

func AddRssAggregator(agg *types.Aggregator, db *pg.DB) (int64, error) {
	var id int64
	_, err := db.Model(agg).
		Returning("id").
		Insert(pg.Scan(&id))
	return id, err
}
