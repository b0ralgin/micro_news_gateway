package services

import (
	"context"
	"time"

	task "bitbucket.org/b0ralgin/micro_API/task"
	"bitbucket.org/b0ralgin/micro_news_gateway/types"
	micro "github.com/micro/go-micro"
)

func RunAggregator(aggreagtor types.Aggregator, client micro.Publisher) error {
	c := time.NewTicker(time.Duration(aggreagtor.Update * int64(time.Second)))
	for _ = range c.C {
		p := task.Task{
			Url:             aggreagtor.Url,
			Type:            aggreagtor.Type,
			Timestamp:       time.Now().Unix(),
			LinkSelector:    aggreagtor.LinkSelector,
			TitleSelector:   aggreagtor.TitleSelector,
			ContentSelector: aggreagtor.ContentSelector,
			PublishSelector: aggreagtor.PublishedAtSelector,
		}
		if err := client.Publish(context.TODO(), &p); err != nil {
			return err
		}
	}
	return nil
}
