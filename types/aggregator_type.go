package types

type Aggregator struct {
	ID                  int64 `json:"-"`
	Type                string
	Url                 string `json:"url"`
	Update              int64  `json:"refresh"`
	LinkSelector        string `json:"link_selector"`
	TitleSelector       string `json:"title_selector"`
	ContentSelector     string `json:"content_selector"`
	PublishedAtSelector string `json:"publish_selector"`
}

func (a *Aggregator) Valid() bool {
	return a.Type == "rss" &&
		a.Url != "" &&
		a.Update > 0
}

func (a *Aggregator) HtmlValid() bool {
	return a.Valid() &&
		a.LinkSelector != "" &&
		a.TitleSelector != "" &&
		a.ContentSelector != ""
}
