package main

import (
	"flag"
	"log"
	"os"
	"time"

	"bitbucket.org/b0ralgin/micro_news_gateway/config"
	"bitbucket.org/b0ralgin/micro_news_gateway/controllers"
	pg "github.com/go-pg/pg"
	"github.com/labstack/echo"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-plugins/broker/nats"
	natsio "github.com/nats-io/nats"
)

var (
	cnfFilename = flag.String("config", "config/config.yaml", "Filename of config file (YML format)")
)

func main() {
	flag.Parse()

	e := echo.New()
	config, err := config.LoadCfg(*cnfFilename)
	if err != nil {
		log.Fatal("cannot parse config file:", err)
	}

	broker := nats.NewBroker(nats.Options(natsio.Options{
		Url: config.NatsURL,
	}))
	service := micro.NewService(
		micro.Name("go.micro.gateway"),
		micro.Broker(broker),
	)
	service.Init()

	db, err := openDB(config.DB, config.LogMode)
	rssPub := micro.NewPublisher(config.RssTaskQueue, service.Client())
	htmlPub := micro.NewPublisher(config.HtmlTaskQueue, service.Client())
	pubs := map[string]micro.Publisher{
		"rss":  rssPub,
		"html": htmlPub,
	}
	taskController := controllers.NewTaskController(db, pubs)
	articleController := controllers.NewArticleController(db, service.Client())

	e.POST("/rss", taskController.AddRssAggregator)
	e.POST("/html", taskController.AddHtmlAggregator)
	e.GET("/news", articleController.FindBy(config.PerPage))

	log.Fatal(e.Start(config.Port))
}

func openDB(conn string, debugMode bool) (*pg.DB, error) {
	opts, err := pg.ParseURL(conn)
	if err != nil {
		return nil, err
	}
	db := pg.Connect(opts)

	if debugMode {
		logger := log.New(os.Stdout, "db", log.LstdFlags)
		pg.SetLogger(logger)
	}
	var st time.Time
	_, err = db.QueryOne(pg.Scan(&st), "set timezone TO 'GMT'; SELECT now()")
	return db, err
}
