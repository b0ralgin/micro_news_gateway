package config

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	Port          string `yaml:"port"`
	DB            string `yaml:"db"`
	LogMode       bool   `yaml:"log_mode"`
	NatsURL       string `yaml:"nats_url"`
	RssTaskQueue  string `yaml:"rss_task_queue"`
	HtmlTaskQueue string `yaml:"html_task_queue"`
	PerPage       int    `yaml:"per_page"`
}

func LoadCfg(fileName string) (*Config, error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	cfg := Config{}
	err = yaml.Unmarshal(file, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
